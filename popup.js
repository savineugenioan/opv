document.addEventListener('DOMContentLoaded', function () {
    chrome.storage.local.set({'links': ""}, function() {
        //console.log(links);
      });
    chrome.tabs.executeScript({
        file: 'myscript1.js'
    }, function () {

        chrome.storage.local.get(['links'], function (result) {
            if(result.links ==""){
                var para = document.createElement("p");
                //para.classList.add("example_a");
                para.style.color= "white";
                para.style.width= "100px";
                para.style.fontSize= "large";
                var node = document.createTextNode("I found no link to open");
                para.appendChild(node);
                var element = document.getElementById("1");
                element.appendChild(para);
                return;
            }
            let link = result.links;
            let urls = [];
            if(link.indexOf(',')>0){
                urls = link.split(',');
            }
            else{
                urls[0] = String(link);
            }
            for (let i = 0; i < urls.length; i++)
            {
                urls[i] = mytrim(urls[i]);
                var para = document.createElement("button");
                para.classList.add("example_a");
                var node = document.createTextNode(urls[i]);
                para.appendChild(node);
                var element = document.getElementById("1");
                element.appendChild(para);
            
            para.addEventListener('click', function () {
                window.open(urls[i])
            });
        }
        });
    })
});

function mytrim(text){
    if(text.startsWith("http://") || text.startsWith("https://") || text.startsWith("blob:")){
                return text;
    }
    else{
        for(let i=0;i<text.length;i++)
            if(text[i].toUpperCase() != text[i].toLowerCase()){
                text = text.replace(text.substring(0,i),"https://");
                break;
            }
        return text;
    }
}
